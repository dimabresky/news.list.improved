# news.list.improved

Улучшенный компонент news.list для 1C-Битрикc: Управление сайтом.

Для установки данного компонента клонируйте или распакуйте архив с репозиторием в свое пространство имен на сайте. После этого компонент
появится в структуре дерева компонентов в визуальном редакторе:

![Дерево компонентов виз. редактора](https://gitlab.com/dimabresky/news.list.improved/raw/master/images/64207f4de2.jpg) 

Данный компонент наследует все возможности стандартного компонента news.list и превносит в него следующие улучшения:

1. Теперь в шаблон компонента можно добавить файл component_prolog.php. Данный файл будет автоматически подключен для 
данного шаблона перед выполнением основного тела компонента (component.php) и как следует до начала кеширования компонента.

2. Теперь появилась возможность задать фильтр для выборки по свойствам инфоблока прямо из формы редактирования компонента!!!
(подобно тому, как программисты это делают, определяя ключ в массиве $GLOBALS перед подключением компонента). Для этого служит секция 
"Дополнительные параметры фильтрации для вывода элементов":

![Дополнительные параметры фильтрации для вывода элементов в форме редактирования параметров компонента](https://gitlab.com/dimabresky/news.list.improved/raw/master/images/1af3bb8c2d.jpg) 